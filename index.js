import component from "svelte-tag"

export function initSvelte(imports, skipFromNamespacing, options = {}) {

    let checkDoubles = {}
    let folderSeparator = '--'
    if (options.folderSeparator) {
        folderSeparator = options.folderSeparator;
    }

    for (const path in imports) {

        // skip n from namespacing

       const p = path.split('/')
        p.splice(0,skipFromNamespacing)

        // skip additional folder from namespacing

        if (options.exclude) {
            if (options.exclude.includes(p[0])) {
                p.splice(0,1)
            }
        }

        // build tagName
        const tn = p.join(folderSeparator)
        const tagName = tn.match(/^[\s\S]+(?=.svelte)/)[0]

        if (options.debug) {
            console.debug('SVELTE TAG «' + tagName + '» from: ' + path)
        }

        // check

        if ( tagName in checkDoubles ) {
            checkDoubles[tagName].push(path)
            const err = "NAMING CONFLICT SVELTE\nDouble identifier: «" + tagName + "»\n\nfrom:\n" + checkDoubles[tagName].join("\n")
            console.error(err)
            alert(err)
        } else {
            checkDoubles[tagName] = [path]
        }

        const app = imports[path].default;
        new component({component: app, tagname: tagName})
    }
}
