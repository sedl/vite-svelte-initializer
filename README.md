This initializer builds svelte components as custom elements based on the filename. Why this way? See [description on dev.to](https://dev.to/chmich/setup-inertia-and-svelte-on-rails-7-3glk).

On Rails we are familiar with nice and handy conventions that saving a lot of time.

At [this tutorial](https://dev.to/chmich/setup-vite-svelte-inertia-stimulus-bootstrap-foundation-on-rails-7-overview-1bk1) i showed a way for integrating Svelte and Stimulus on rails.

On the [Handbook](https://stimulus.hotwired.dev/handbook/installing) Stimulus has shown a default way for declaring keys based of the controller filenames.  While this is a safe way i found this double-dashes silly and the namespaces could be shortened. Of course, if you want to have a naming where you can be there is never a conflict, please stay at this default way. That is what this module, by default, does. 

For a smarter naming this module adds some options.

This corresponds with [vite-stimulus-initializer](https://gitlab.com/sedl/vite-stimulus-initializer).

If a naming conflict occours, it alerts on console with a log entry and on Browser with a alert. 

Assume a file structure like so:

`frontend/javascript/`

```
- components
   |-- articles
      |-- select.svelte
- global
   |-- select.svelte
```

## dependencies

[svelte-tag](https://www.npmjs.com/package/svelte-tag)

## default: the safe way

`frontend/entrypoints/application.js`

```
import { initSvelte } from "vite-svelte-initializer";

const apps = import.meta.glob('../javascript/components/**/*.svelte', { eager: true })
initSvelte(apps, 2)
```

=> "2", the second parameter that the first two parts of the part (".." and "javascript" are excluded from namespacing, so it generates a custom element with the tagname:

`components--articles--select`, which can be applied as custom element in the view by `<components--articles--select />`, and a `global--select` tag.

## optional: the smarter way

for making that more handy, you can:

`frontend/entrypoints/application.js`

```
import { initSvelte } from "vite-svelte-initializer";

const apps = import.meta.glob('../javascript/components/**/*.svelte', { eager: true })
initSvelte(apps, 2, { debug: true, exclude: ['components'], folderSeparator: '-' })
```

this prints out on console (because of `debug: true`):

- `SVELTE TAG «articles-select» from: ./components/articles/select.svelte`
- `SVELTE TAG «global-select» from: ./global/select.svelte`

and having no more double-dash because of `folderSeparator: '-'`

The `exclude: [..]` is only applied for the level-3, which means: the first two levels are excluded completely because of second parameter, and the next is targeted by `exclude`.

and generates the corresponding tags.

Hope you enjoy!
